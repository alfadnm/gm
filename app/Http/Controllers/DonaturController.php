<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Donatur;
use App\User;


class DonaturController extends Controller
{
    public function dataDonatur(){
    	$data = Donatur::orderBy('updated_at','desc')->get();
    	return view('Donatur.dataDonatur')-> with('data', $data);
    }

    public function tampilanDonatur(){
    	return view('Donatur.createDonatur');
    }

    
    public function tambahDonatur( Request $request){
    	$this->validate($request, [
    		'emailDonatur' => 'required|unique:users,email',
            'password' => 'required|min:8',
            'namaDonatur' => 'required',
            'alamat' => 'required',
            'telepon' => 'required|min:6',
            'tanggalLahir' => 'required',
			'jenisKelamin' => 'required',
			'pekerjaan' => 'required',
			'instansi' => 'required',
			'alamatInstansi' => 'required',
			'kartuIdentitas' => 'required',
			'noKartuIdentitas' => 'required',
			'checkbox' => 'required'
        ],[
        	'emailDonatur.required' => 'Email harus diisi',
        	'emailDonatur.unique' => 'Email sudah terdaftar',
        	'password.required' => 'password harus diisi',
        	'password.min'=>'Password minimal 8 karakter',
        	'namaDonatur.required' => 'Nama harus diisi',
        	'alamat.required' => 'ALamat harus diisi',
        	'telepon.required' => 'Telepon harus diisi',
        	'telepon.min'=>'Telepon minimal 8 karakter',
        	'tanggalLahir.required' => 'Tanggal Lahir harus diisi',
        	'jenisKelamin.required' => 'Jenis Kelamin harus diisi',
        	'pekerjaan.required' => 'Pekerjaan harus diisi',
        	'instansi.required' => 'Nama Instansi harus diisi',
        	'alamatInstansi.required' => 'Alamat Instansi harus diisi',
        	'kartuIdentitas.required' => 'Jenis Kartu identitas harus diisi',
        	'noKartuIdentitas.required' => 'Nomor kartu Identitas harus diisi',
        	'checkbox.required' => 'Silahkan centang untuk memastikan data yang Anda isikan benar'


        ]);


    	$user = new User;
    	$user -> email =Input::get('emailDonatur');
    	$user -> password =Input::get('password');
    	$user -> save();

    	$data = new Donatur ;

    	$data->namaDonatur = Input::get('namaDonatur');
		$data->idUser = $user->id ;
		$data->alamat = Input::get('alamat');
		$data->telepon = Input::get('telepon');
		$data->jenisKelamin = Input::get('jenisKelamin');
		$data->tanggalLahir = \Carbon\Carbon::parse(Input::get('tanggalLahir')) -> format('Y-m-d');
		$data->pekerjaan = Input::get('pekerjaan');
		$data->instansi = Input::get('instansi');
		$data->alamatInstansi = Input::get('alamatInstansi');
		$data->kartuIdentitas = Input::get('kartuIdentitas');
		$data->noKartuIdentitas = Input::get('noKartuIdentitas');
		
		
		$data->save();
		$notification= array(
			'message' => 'Data Donatur berhasil ditambahkan',
			'alert-type'=>'success');

		return redirect('data-donatur')->with($notification);

    }

    public function edit($id)
	{
		$data = Donatur::find($id);
		return view('donatur.edit')->with('data',$data);
	}

    public function update($id, Request $request)
	{
		$validator = Validator::make($request->all(), [
            'namaDonatur' => 'required',
            'alamat' => 'required',
            'telepon' => 'required',
            'tanggalLahir' => 'required',
			'jenisKelamin' => 'required',
			'pekerjaan' => 'required',
			'instansi' => 'required',
			'alamatInstansi' => 'required',
			'kartuIdentitas' => 'required',
			'noKartuIdentitas' => 'required'
        ]);
		
		if ($validator->fails()) {
            return redirect('edit/'.$id)
				->withErrors($validator)
				->withInput();
        }
		
		$data = Donatur::find($id);
		
		// $data->nama_field_di_database = Input::get('nama_field_di_form');
		
		$data->namaDonatur = Input::get('namaDonatur');
		$data->alamat = Input::get('alamat');
		$data->telepon = Input::get('telepon');
		$data->jenisKelamin = Input::get('jenisKelamin');
		$data->tanggalLahir = \Carbon\Carbon::parse(Input::get('tanggalLahir')) -> format('Y-m-d');
		$data->pekerjaan = Input::get('pekerjaan');
		$data->instansi = Input::get('instansi');
		$data->alamatInstansi = Input::get('alamatInstansi');
		$data->kartuIdentitas = Input::get('kartuIdentitas');
		$data->noKartuIdentitas = Input::get('noKartuIdentitas');
		

		$data->save();
		
		$user=User::find($data->idUser);
		$user->email=Input::get('emailDonatur');
		$user->save();
		return redirect('data-donatur')->with('sukses_update','yes');

	}
	public function delete($id){
		$data = Donatur::find($id);
		$user=User::find($data->idUser);
		$data->delete();
		$user->delete();

		return redirect('data-donatur')->with('sukses_update','yes');
	}

	public function detail($id){
		$data = Donatur::find($id);
		return view('donatur.detail')->with('data',$data);
	}
}
