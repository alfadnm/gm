<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Mustahik;
use App\User;


class MustahikController extends Controller
{
    public function dataMustahik(){
    	$data = Mustahik::orderBy('updated_at','desc')->get();
    	return view('Mustahik.dataMustahik')-> with('data', $data);
    }

    public function tampilanMustahik(){
    	return view('Mustahik.createMustahik');
    }

     
    public function tambahMustahik( Request $request){
    	$this->validate($request, [
    		
            'namaMustahik' => 'required',
            'alamatMustahik' => 'required',
            'teleponMustahik' => 'required|min:8',
            'pekerjaanMustahik' => 'required',
			'checkbox' => 'required'
        ],[
        	'namaMustahik.required' => 'Nama harus diisi',
        	'alamatMustahik.required' => 'ALamat harus diisi',
        	'pekerjaanMustahik.required' => 'Pekerjaan harus diisi',
        	'teleponMustahik.required' => 'Telepon harus diisi',
        	'teleponMustahik.min'=>'Telepon minimal 8 karakter',
        	
        	'checkbox.required' => 'Silahkan centang untuk memastikan data yang Anda isikan benar'


        ]);


    	$data = new Mustahik ;

    	$data->namaMustahik = Input::get('namaMustahik');
		$data->alamatMustahik = Input::get('alamatMustahik');
		$data->teleponMustahik = Input::get('teleponMustahik');
		$data->pekerjaanMustahik = Input::get('pekerjaanMustahik');
		
		
		
		$data->save();
		$notification= array(
			'message' => 'Data Mustahik berhasil ditambahkan',
			'alert-type'=>'success');

		return redirect('data-mustahik')->with($notification);

    }


    public function editMustahik($id)
	{
		$data = Mustahik::find($id);
		return view('Mustahik.editMustahik')->with('data',$data);
	}

    public function updateMustahik($id, Request $request)
	{
		$this->validate($request, [
            
            'namaMustahik' => 'required',
            'alamatMustahik' => 'required',
            'teleponMustahik' => 'required|min:6',
            'pekerjaanMustahik' => 'required',
			'checkbox' => 'required'
        ],[
		
			'namaMustahik.required' => 'Nama harus diisi',
        	'alamatMustahik.required' => 'ALamat harus diisi',
        	'pekerjaanMustahik.required' => 'Pekerjaan harus diisi',
        	'teleponMustahik.required' => 'Telepon harus diisi',
        	'teleponMustahik.min'=>'Telepon minimal 6 karakter',
        	
        	'checkbox.required' => 'Silahkan centang untuk memastikan data yang Anda isikan benar'


        ]);

		$data = Mustahik::find($id);
		
		// $data->nama_field_di_database = Input::get('nama_field_di_form');
		
		$data->namaMustahik = Input::get('namaMustahik');
		$data->alamatMustahik = Input::get('alamatMustahik');
		$data->teleponMustahik = Input::get('teleponMustahik');
		$data->pekerjaanMustahik = Input::get('pekerjaanMustahik');
		

		$data->save();
		
		
		return redirect('data-mustahik')->with('sukses_update','yes');

	}

    public function deleteMustahik($id){
		$data = Mustahik::find($id);
		$data->delete();
		

		return redirect('data-mustahik')->with('sukses_update','yes');
	}

	public function dataDonasi(){
		$data = Donasi::orderBy('updated_at','desc')->get();
    	return view('Mustahik.dataDonasi')-> with('data', $data);
    }
    
}
