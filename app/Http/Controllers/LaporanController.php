<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Models\Penerimaan;
use App\Models\Penyaluran;
use App\Models\Jurnal;
use App\Models\Rekening;
use App\User;

class LaporanController extends Controller
{
	//penerimaan
     public function tampilLaporanPenerimaan(){
    	$data = Penerimaan::orderBy('updated_at','desc')->get();
    	return view('Laporan.penerimaan')-> with('data', $data);
    }


    //penyaluran
    public function tampilLaporanPenyaluran(){
    	$data = Penyaluran::orderBy('updated_at','desc')->get();
    	return view('Laporan.penyaluran')-> with('data', $data);;
    }

    //jurnal
    public function tampilJurnal(){
    	$data = Jurnal::orderBy('updated_at','desc')->get();
    	return view('Laporan.jurnal')-> with('data', $data);;
    }

    //rekening

    public function tampilRekening(){
    	$data = Rekening::orderBy('updated_at','desc')->get();
    	return view('Laporan.rekening')-> with('data', $data);;
    }

   
}
