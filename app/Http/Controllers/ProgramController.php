<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Program;



class ProgramController extends Controller
{
   

    public function tampilProgram(){
    	
    	$data = Program::orderBy('updated_at','desc')->get();
    	return view('Program.pilihanProgram')-> with('data', $data);
    }

    public function tampilanTambahProgram(){
    	return view('Program.tambahProgram');
    }

     public function tambahProgram( Request $request){
    	
     	$this->validate($request, [
    		'kodeProgram' => 'required',
			'namaProgram' => 'required',
			'targetDana' => 'required',
			'idRekening' => 'required',
			
        ],[
        	'kodeProgram.required' => 'Kode Program harus diisi',
        	'namaProgram.unique' => 'nama Program sudah terdaftar',
        	'targetDana.required' => 'target Dana harus diisi',
        	'idRekening.min'=>'ID Rekening minimal 8 karakter',
        	
        	'checkbox.required' => 'Silahkan centang untuk memastikan data yang Anda isikan benar'


        ]);

    	
    	$data = new Program;

    	$data->kodeProgram = Input::get('kodeProgram');
		$data->namaProgram = Input::get('namaProgram');
		$data->targetDana = Input::get('targetDana');
		$data->idRekening = Input::get('idRekening');
		$data->keterangan = Input::get('keterangan');

		$data->save();
		$notification= array(
			'message' => 'Data Program berhasil ditambahkan',
			'alert-type'=>'success');

		return redirect('pilihan-program')->with($notification);

    }

    public function editProgram($id)
	{
		$data = Program::find($id);
		return view('Program.editProgram')->with('data',$data);
	}

	public function deleteProgram($id){
			$data = Program::find($id);
			$data->delete();
			
			return redirect('pilihan-program')->with('sukses_update','yes');
		}

	public function detailProgram($id){
		$data = Program::find($id);
		return view('Program.detailProgram')->with('data',$data);
	}

 	public function updateProgram($id, Request $request)
	{
		$validator = Validator::make($request->all(), [
            'kodeProgram' => 'required',
			'namaProgram' => 'required',
			'targetDana' => 'required',
			'idRekening' => 'required',
        ]);
		
		if ($validator->fails()) {
            return redirect('create')
				->withErrors($validator)
				->withInput();
        }
        $data = Program::find($id);
        
        $data->kodeProgram = Input::get('kodeProgram');
		$data->namaProgram = Input::get('namaProgram');
		$data->targetDana = Input::get('targetDana');
		$data->idRekening = Input::get('idRekening');
		$data->keterangan = Input::get('keterangan');

		$data->save();
		$notification= array(
			'message' => 'Data Program berhasil ditambahkan',
			'alert-type'=>'success');

		return redirect('pilihan-program')->with($notification);
	}


    
}
