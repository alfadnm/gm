<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donatur extends Model
{
	protected $table = "donatur";
	public static function daftar() {

		if (isset($_GET['q'])) {
			$keyword = $_GET['q'];
			$data = Donatur::where('origin','like','%'. $keyword.'%')
			->paginate(10);
		} else {
			$data = Donatur::paginate(10);
		}

		return $data;

	}

public function user(){
	return $this->belongsTo('App\User','idUser');
}

}