<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mustahik extends Model
{
	protected $table = "mustahik";
	public static function daftar() {

		if (isset($_GET['q'])) {
			$keyword = $_GET['q'];
			$data = Mustahik::where('origin','like','%'. $keyword.'%')
			->paginate(10);
		} else {
			$data = Mustahik::paginate(10);
		}

		return $data;

	}



}