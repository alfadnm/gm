<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
	protected $table = "jurnal";
	public static function jurnal() {

		if (isset($_GET['q'])) {
			$keyword = $_GET['q'];
			$data = Jurnal::where('origin','like','%'. $keyword.'%')
			->paginate(10);
		} else {
			$data = Jurnal::paginate(10);
		}

		return $data;

	}



}