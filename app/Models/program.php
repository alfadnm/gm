<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $table = "program";
	public static function program() {

		if (isset($_GET['q'])) {
			$keyword = $_GET['q'];
			$data = Program::where('origin','like','%'. $keyword.'%')
			->paginate(10);
		} else {
			$data = Program::paginate(10);
		}

		return $data;

	}



}