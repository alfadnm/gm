<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penyaluran extends Model
{
	protected $table = "penyaluran";
	public static function penyaluran() {

		if (isset($_GET['q'])) {
			$keyword = $_GET['q'];
			$data = Penyaluran::where('origin','like','%'. $keyword.'%')
			->paginate(10);
		} else {
			$data = Penyaluran::paginate(10);
		}

		return $data;

	}



}