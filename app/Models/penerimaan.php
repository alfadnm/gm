<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penerimaan extends Model
{
	protected $table = "penerimaan";
	public static function penerimaan() {

		if (isset($_GET['q'])) {
			$keyword = $_GET['q'];
			$data = Penerimaan::where('origin','like','%'. $keyword.'%')
			->paginate(10);
		} else {
			$data = Penerimaan::paginate(10);
		}

		return $data;

	}



}