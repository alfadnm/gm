<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/home', 'HomeController@index');

// Route::get('/', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');
// Route::get('/','ChatsController@tes');

Route::get('/','DashboardController@dashboard');


//Layanan Donatur
//Donatur
Route::get('/data-donatur','DonaturController@dataDonatur');
Route::get('/tambah-donatur','DonaturController@tampilanDonatur');

Route::post('/tambahDonatur','DonaturController@tambahDonatur');
Route::get('edit/{id}', 'DonaturController@edit');
Route::post('update/{id}', 'DonaturController@update');
Route::get('delete/{id}', 'DonaturController@delete');
Route::get('detail/{id}', 'DonaturController@detail');

//Pilihan Program
Route::get('/pilihan-program','ProgramController@tampilProgram');
Route::get('/tampilan-program','ProgramController@tampilanTambahProgram');
Route::post('/tambah-program','ProgramController@tambahProgram');
Route::get('delete-program/{id}', 'ProgramController@deleteProgram');
Route::get('edit-program/{id}', 'ProgramController@editProgram');
Route::post('update-program/{id}', 'ProgramController@updateProgram');

//Layanan Mustahik
//Data Mustahik

Route::get('/data-mustahik','MustahikController@dataMustahik');
Route::get('/tambah-mustahik','MustahikController@tampilanMustahik');

Route::post('/tambahMustahik','MustahikController@tambahMustahik');
Route::get('edit-mustahik/{id}', 'MustahikController@editMustahik');
Route::post('update-mustahik/{id}', 'MustahikController@updateMustahik');
Route::get('delete-mustahik/{id}', 'MustahikController@deleteMustahik');
Route::get('detail-mustahik/{id}', 'MustahikController@detailMustahik');

//DataDonasi

Route::post('/data-donasi','MustahikController@dataDonasi');


Route::get('/jurnal','LaporanController@tampilJurnal');
Route::get('/laporan-penerimaan','LaporanController@tampilLaporanPenerimaan');
Route::get('/laporan-penyaluran','LaporanController@tampilLaporanPenyaluran');
Route::get('/rekening','LaporanController@tampilRekening');

Route::get('/jenis-donasi','ManajemensistemController@tampilJenisDonasi');
Route::get('/ekspor-import','ManajemensistemController@tampilEksportImport');

Route::get('/profil','ProfilController@tampilProfil');
