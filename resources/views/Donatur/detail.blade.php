
@extends('layouts.app')
@section('content')

  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Donatur
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Donatur</a></li>
            <li><a href="{{url('data-donatur')}}">Data Donatur</a></li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ url('update/'.$data->id)}}" method="post">
                  <div class="box-body">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="emailDonatur" value='{{$data->user->email}}' disabled>
                    </div>
                    
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="namaDonatur" value='{{$data->namaDonatur}}' disabled>
                    </div>
                    
                    <div class="form-group">
                    <label>Jenis Kelamin</label>
                       <input type="text" class="form-control" placeholder="Enter ..." name="jenisKelamin" value='{{$data->jenisKelamin}}' disabled>
                    </div>
                    <div class="form-group">
                   <label >Tanggal Lahir</label>
                    
                    <input type="text" data-provide="datepicker" class="form-control" name="tanggalLahir" value='{{\Carbon\Carbon::parse($data->tanggalLahir)->format("d M Y")}}' id='Tanggal' disabled>
                    </div>

                                        <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamat" disabled>{{$data->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Nomor Telepon</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="telepon" value='{{$data->telepon}}' disabled>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Kartu Identitas</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="jenisKelamin" value='{{$data->kartuIdentitas}}' disabled>
                    </div>
                    <div class="form-group">
                      <label>Nomor Identitas</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="noKartuIdentitas" value='{{$data->noKartuIdentitas}}' disabled>
                    </div>
                    <div class="form-group">
                      <label>Pekerjaan</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="pekerjaan" value='{{$data->pekerjaan}}' disabled>
                    </div>
                    <div class="form-group">
                      <label>Nama Instansi</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="instansi" value='{{$data->instansi}}' disabled>
                    </div>
                    <div class="form-group">
                      <label>Alamat Instansi</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamatInstansi" disabled>{{$data->alamatInstansi}}</textarea>
                    </div>
                    
                    
                    </div>
                  </div><!-- /.box-body -->
                  
                </form>
              </div><!-- /.box -->
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
                    
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
              </form>
              </div>
              </div>


            
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- /.content-wrapper -->

  


</div>
@endsection
