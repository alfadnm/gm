@extends('layouts.app')
@section('title', 'Tambah Donatur')
@section('data-donatur','active')
@section('donatur','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Donatur
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Donatur</a></li>
            <li><a href="#">Data Donatur</a></li>
            <li class="active">Tambah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ url('tambahDonatur')}}" method="post">
                  <div class="box-body">
                  <div class="col-md-6">
                    <div class="form-group {{ $errors->has('emailDonatur') ? ' has-error' : '' }}">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="emailDonatur" value="{{ old('emailDonatur') }}">
                      @if ($errors->has('emailDonatur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('emailDonatur') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" >
                      @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('namaDonatur') ? ' has-error' : '' }}">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="namaDonatur" value="{{ old('namaDonatur') }}">
                       @if ($errors->has('namaDonatur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('namaDonatur') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    <div class="form-group {{ $errors->has('jenisKelamin') ? ' has-error' : '' }}">
                    <label>Jenis Kelamin</label>
                      <div class="radio">

                        <label>
                          <input type="radio" name="jenisKelamin" id="optionsRadios1" value="L" @if(old('jenisKelamin')=='L') checked @endif >
                          Laki-laki
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="jenisKelamin" id="optionsRadios2" value="P" @if(old('jenisKelamin')=='P') checked @endif >
                          Perempuan
                        </label>
                      </div>
                       @if ($errors->has('jenisKelamin'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenisKelamin') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('tanggalLahir') ? ' has-error' : '' }}">
                   <label >Tanggal Lahir</label>
                    <input type="text"  class="form-control" name="tanggalLahir"  id='Tanggal' value="{{ old('tanggalLahir') }}">
                     @if ($errors->has('tanggalLahir'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggalLahir') }}</strong>
                        </span>
                      @endif
                    </div>

                    <div class="form-group {{ $errors->has('alamat') ? ' has-error' : '' }}">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamat">{{ old('alamat') }}</textarea>
                       @if ($errors->has('alamat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('telepon') ? ' has-error' : '' }}">
                      <label>Nomor Telepon</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="telepon" value="{{ old('telepon') }}">
                       @if ($errors->has('telepon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telepon') }}</strong>
                        </span>
                      @endif
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('kartuIdentitas') ? ' has-error' : '' }}">
                      <label>Kartu Identitas</label>
                      <select name="kartuIdentitas">
                      <option value= "KTP" @if(old('kartuIdentitas')=='KTP') selected @endif >KTP</option>
                      <option value= "SIM" @if(old('kartuIdentitas')=='SIM') selected @endif>SIM</option>
                      <option value= "lainnya" @if(old('kartuIdentitas')=='lainnya') selected @endif> Lainnya</option>
                      </select>
                       @if ($errors->has('kartuIdentitas'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kartuIdentitas') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('noKartuIdentitas') ? ' has-error' : '' }}">
                      <label>Nomor Identitas</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="noKartuIdentitas" value="{{ old('noKartuIdentitas') }}">
                       @if ($errors->has('noKartuIdentitas'))
                        <span class="help-block">
                            <strong>{{ $errors->first('noKartuIdentitas') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('pekerjaan') ? ' has-error' : '' }}">
                      <label>Pekerjaan</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="pekerjaan" value="{{ old('pekerjaan') }}">
                       @if ($errors->has('pekerjaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pekerjaan') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('instansi') ? ' has-error' : '' }}">
                      <label>Nama Instansi</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="instansi" value="{{ old('instansi') }}">
                       @if ($errors->has('instansi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('instansi') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('alamatInstansi') ? ' has-error' : '' }}">
                      <label>Alamat Instansi</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamatInstansi">{{ old('alamatInstansi') }}</textarea>
                       @if ($errors->has('alamatInstansi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamatInstansi') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    <div class="form-group {{ $errors->has('checkbox') ? ' has-error' : '' }}">
                      
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="checkbox"> Data yang saya isikan sudah benar,
                          </label>
                        </div>
                         @if ($errors->has('checkbox'))
                        <span class="help-block">
                            <strong>{{ $errors->first('checkbox') }}</strong>
                        </span>
                      @endif
                     
                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info pull-right">Tambah</button>
                    <button  class="btn btn-default pull-right" style="margin-right:10px">Cancel</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
                    
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
              </form>
              </div>
              </div>


            
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- /.content-wrapper -->

 
</div>
<!-- ./wrapper -->
@endsection
@section('script')
<script type="text/javascript">
  //Date picker
    $('#Tanggal').datepicker({
      autoclose: true, 
      format :'dd MM yyyy',
      language : 'id',
    });


</script>
@endsection
