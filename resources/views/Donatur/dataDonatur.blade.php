@extends('layouts.app')

@section('title', 'data-donatur')
@section('data-donatur','active')
@section('donatur','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Donatur
        <small>Data donatur yang masuk</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Donatur</a></li>
        
        <li class="active">Data Donatur</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <a href= "{{ url('tambah-donatur')}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i>  Tambah Donatur
              </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>No Telepon</th>
                    <th>Pekerjaan</th>
                    <th> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                @foreach($data as $q=>$donatur)
                  <tr>
                    <td>{{$q+1}} </td>
                    <td>{{$donatur->namaDonatur}}</td>
                    <td>{{$donatur->jenisKelamin}}</td>
                    <td> {{$donatur->telepon}}</td>
                    <td>{{$donatur->pekerjaan}}</td>
                    <td>
                     <div class="btn-group" role="group" aria-label="...">
                        <a href="{{ url('detail/'.$donatur->id) }}" class="btn btn-sm btn-success">Detail</a>
                        <a href="{{ url('edit/'.$donatur->id) }}" class="btn btn-sm btn-warning" style="margin-left: 5px">Edit</a>
                      
                        <a href="{{ url('delete/'.$donatur->id) }}"" onclick="return confirm('Are you sure to delete this data?')" class="btn btn-sm btn-danger" style="margin-left: 5px">Hapus</a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
  <script >
     $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  </script>
@endsection

