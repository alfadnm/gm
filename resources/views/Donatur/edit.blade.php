
@extends('layouts.app')
@section('content')

  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Donatur
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Donatur</a></li>
            <li><a href="{{url('data-donatur')}}">Data Donatur</a></li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ url('update/'.$data->id)}}" method="post">
                  <div class="box-body">
                  <div class="col-md-6">
                    <div class="form-group {{ $errors->has('emailDonatur') ? ' has-error' : '' }}">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="emailDonatur" value="@if(old ('emailDonatur') != null) {{old ('emailDonatur') }} @else {{ $data->emailDonatur}} @endif">
                       @if ($errors->has('emailDonatur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('emailDonatur') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                      @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('namaDonatur') ? ' has-error' : '' }}">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="namaDonatur" value="@if(old ('namaDonatur') != null) {{old ('namaDonatur') }} @else {{ $data->namaDonatur}} @endif">
                       @if ($errors->has('namaDonatur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('namaDonatur') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    <div class="form-group {{ $errors->has('jenisKelamin') ? ' has-error' : '' }}">
                    <label>Jenis Kelamin</label>
                      <div class="radio">

                        <label>
                          <input type="radio" name="jenisKelamin" id="optionsRadios1" @if(old('jenisKelamin') != null) @if(old('jenisKelamin') == 'L') checked @endif @else @if($data->jenisKelamin=="L") checked @endif @endif value="L">
                          Laki-laki
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="jenisKelamin" id="optionsRadios2" @if(old('jenisKelamin') != null) @if(old('jenisKelamin') == 'P') checked @endif @else @if($data->jenisKelamin=="P") checked @endif @endif value="P">
                          Perempuan
                        </label>
                      </div>
                      @if ($errors->has('jenisKelamin'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenisKelamin') }}</strong>
                        </span>
                      @endif
                    </div>

                    <div class="form-group {{ $errors->has('tanggalLahir') ? ' has-error' : '' }}">
                   <label >Tanggal Lahir</label>
                    
                    <input type="text"  class="form-control" name="tanggalLahir" value="@if(old ('tanggalLahir') != null) {{old ('tanggalLahir') }} @else {{\Carbon\Carbon::parse($data->tanggalLahir)->format('d M Y')}} @endif" id='Tanggal'>
                    @if ($errors->has('tanggalLahir'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggalLahir') }}</strong>
                        </span>
                      @endif
                    </div>

                    <div class="form-group {{ $errors->has('alamat') ? ' has-error' : '' }}">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamat">@if(old ('alamat') != null) {{old ('alamat') }} @else {{ $data->alamat}} @endif</textarea>
                       @if ($errors->has('alamat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('telepon') ? ' has-error' : '' }}">
                      <label>Nomor Telepon</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="telepon" value="@if(old ('telepon') != null) {{old ('telepon') }} @else {{ $data->telepon}} @endif">
                      @if ($errors->has('telepon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telepon') }}</strong>
                        </span>
                      @endif
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('kartuIdentitas') ? ' has-error' : '' }}">
                      <label>Kartu Identitas</label>
                      <select name="kartuIdentitas">
                      <option @if(old('kartuIdentitas') != null) @if(old('kartuIdentitas') == 'KTP') selected @endif @else @if($data->kartuIdentitas=="KTP") selected @endif @endif value="KTP">KTP</option>

                      <option @if(old('kartuIdentitas') != null) @if(old('kartuIdentitas') == 'SIM') selected @endif @else @if($data->kartuIdentitas=="SIM") selected @endif @endif  value= "SIM" >SIM</option>

                      <option value= "lainnya"> Lainnya</option>
                      </select>
                      @if ($errors->has('kartuIdentitas'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kartuIdentitas') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('noKartuIdentitas') ? ' has-error' : '' }}">
                      <label>Nomor Identitas</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="noKartuIdentitas" value="@if(old ('noKartuIdentitas') != null) {{old ('noKartuIdentitas') }} @else {{ $data->noKartuIdentitas}} @endif">
                      @if ($errors->has('noKartuIdentitas'))
                        <span class="help-block">
                            <strong>{{ $errors->first('noKartuIdentitas') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('pekerjaan') ? ' has-error' : '' }}">
                      <label>Pekerjaan</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="pekerjaan" value="@if(old ('pekerjaan') != null) {{old ('pekerjaan') }} @else {{ $data->pekerjaan}} @endif">
                       @if ($errors->has('pekerjaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pekerjaan') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('instansi') ? ' has-error' : '' }}">
                      <label>Nama Instansi</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="instansi" value="@if(old ('instansi') != null) {{old ('instansi') }} @else {{ $data->instansi}} @endif">
                      @if ($errors->has('instansi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('instansi') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('alamatInstansi') ? ' has-error' : '' }}">
                      <label>Alamat Instansi</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamatInstansi" >@if(old ('alamatInstansi') != null) {{old ('alamatInstansi') }} @else {{ $data->alamatInstansi}} @endif</textarea>
                       @if ($errors->has('alamatInstansi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamatInstansi') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    <div class="form-group{{ $errors->has('checkbox') ? ' has-error' : '' }}">
                      
                        <div class="checkbox">
                          <label>
                            <input type="checkbox"> Data yang saya isikan sudah benar,
                          </label>
                          @if ($errors->has('checkbox'))
                        <span class="help-block">
                            <strong>{{ $errors->first('checkbox') }}</strong>
                        </span>
                      @endif
                        </div>
                     
                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info pull-right">Simpan</button>
                    <button  class="btn btn-default pull-right" style="margin-right:10px">Cancel</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
                    
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
              </form>
              </div>
              </div>


            
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- /.content-wrapper -->

  


</div>
@endsection
@section('script')
<script type="text/javascript">
  //Date picker
    $('#Tanggal').datepicker({
      autoclose: true, 
      format :'dd MM yyyy',
      language : 'id',
    });


</script>
@endsection