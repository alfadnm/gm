
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <li class="@yield ('Dashboard') treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview @yield('donatur')">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layanan Donatur</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@yield('data-donatur')" ><a href="{{ url('data-donatur') }}"><i class="fa fa-circle-o"></i> Data Donatur</a></li>
            <li><a href="{{ url('pilihan-program') }}"><i class="fa fa-circle-o"></i> Pilihan Donasi</a></li>
            
          </ul>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-files-o"></i> <span>Layanan Mustahik</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('data-mustahik') }}"><i class="fa fa-circle-o"></i> Data Mustahik</a></li>
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Data Donasi</a></li>
            
          </ul>
        </li>
        </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('laporan-penerimaan') }}"><i class="fa fa-circle-o"></i> Penerimaan</a></li>
            <li><a href="{{ url('laporan-penyaluran') }}"><i class="fa fa-circle-o"></i> Penyaluran</a></li>
            <li><a href="{{ url('jurnal') }}"><i class="fa fa-circle-o"></i> Jurnal</a></li>
            <li><a href="{{ url('rekening') }}"><i class="fa fa-circle-o"></i> Rekening</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Pengaturan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
        </li>
        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>