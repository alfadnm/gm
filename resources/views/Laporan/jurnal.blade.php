@extends('layouts.app')

@section('title', 'Data Mustahik')
@section('jurnal','active')
@section('jurnal','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Jurnal
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
        
        <li class="active">Jurnal</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <a href= "{{ url('tambah-mustahik')}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i>  Cetak Jurnal
              </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>ID Rekening</th>
                    <th>Nominal</th>
                    <th>Keterangan</th>
                    
                  </tr>
                </thead>
                <tbody>
                @foreach($data as $q=>$jurnal)
                  <tr>
                    <td>{{$q+1}} </td>
                    <td>{{$jurnal->tanggal}}</td>
                    <td>{{$jurnal->idRekening}}</td>
                    <td> {{$jurnal->nominal}}</td>
                    <td>{{$jurnal->keterangan}}</td>
                    <td>
                     
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
  <script >
     $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  </script>
@endsection

