@extends('layouts.app')

@section('title', 'Data Mustahik')
@section('data-mustahik','active')
@section('mustahik','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Mustahik
        <small>Data mustahik yang masuk</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Mustahik</a></li>
        
        <li class="active">Data Mustahik</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <a href= "{{ url('tambah-mustahik')}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i>  Tambah Mustahik
              </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Telepon</th>
                    <th>Pekerjaan</th>
                    <th>Alamat</th>

                    <th>Aksi</th>
                    
                  </tr>
                </thead>
                <tbody>
                @foreach($data as $q=>$mustahik)
                  <tr>
                    <td>{{$q+1}} </td>
                    <td>{{$mustahik->namaMustahik}}</td>
                    <td>{{$mustahik->teleponMustahik}}</td>
                    <td> {{$mustahik->pekerjaanMustahik}}</td>
                    <td>{{$mustahik->alamatMustahik}}</td>
                    <td>
                     <div class="btn-group" role="group" aria-label="...">
                        
                        <a href="{{ url('edit-mustahik/'.$mustahik->id) }}" class="btn btn-sm btn-warning" style="margin-left: 5px">Edit</a>
                      
                        <a href="{{ url('delete-mustahik/'.$mustahik->id) }}"" onclick="return confirm('Are you sure to delete this data?')" class="btn btn-sm btn-danger" style="margin-left: 5px">Hapus</a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
  <script >
     $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  </script>
@endsection

