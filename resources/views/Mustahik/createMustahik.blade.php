@extends('layouts.app')
@section('title', 'Tambah Donatur')
@section('data-donatur','active')
@section('donatur','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Mustahik
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Mustahik</a></li>
            <li><a href="#">Data Mustahik</a></li>
            <li class="active">Tambah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ url('tambahMustahik')}}" method="post">
                  <div class="box-body">
                  <div class="col-md-6">
                    
                    <div class="form-group {{ $errors->has('namaMustahik') ? ' has-error' : '' }}">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="namaMustahik" value="{{ old('namaMustahik') }}">
                       @if ($errors->has('namaMustahik'))
                        <span class="help-block">
                            <strong>{{ $errors->first('namaMustahik') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                     <div class="form-group {{ $errors->has('teleponMustahik') ? ' has-error' : '' }}">
                      <label>Nomor Telepon</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="teleponMustahik" value="{{ old('teleponMustahik') }}">
                       @if ($errors->has('teleponMustahik'))
                        <span class="help-block">
                            <strong>{{ $errors->first('teleponMustahik') }}</strong>
                        </span>
                      @endif
                    </div>
                      
                    

                    <div class="form-group {{ $errors->has('alamatMustahik') ? ' has-error' : '' }}">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamatMustahik">{{ old('alamatMustahik') }}</textarea>
                       @if ($errors->has('alamatMustahik'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamatMustahik') }}</strong>
                        </span>
                      @endif
                    </div>
                   
                    <div class="form-group {{ $errors->has('pekerjaanMustahik') ? ' has-error' : '' }}">
                      <label>Pekerjaan</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="pekerjaanMustahik" value="{{ old('pekerjaanMustahik') }}">
                       @if ($errors->has('pekerjaanMustahik'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pekerjaanMustahik') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    <div class="form-group {{ $errors->has('checkbox') ? ' has-error' : '' }}">
                      
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="checkbox"> Data yang saya isikan sudah benar,
                          </label>
                        </div>
                         @if ($errors->has('checkbox'))
                        <span class="help-block">
                            <strong>{{ $errors->first('checkbox') }}</strong>
                        </span>
                      @endif
                     
                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info pull-right">Tambah</button>
                    <button  class="btn btn-default pull-right" style="margin-right:10px">Cancel</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
                    
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
              </form>
              </div>
              </div>


            
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- /.content-wrapper -->

 
</div>
<!-- ./wrapper -->
@endsection
@section('script')
<script type="text/javascript">
  //Date picker
    $('#Tanggal').datepicker({
      autoclose: true, 
      format :'dd MM yyyy',
      language : 'id',
    });


</script>
@endsection
