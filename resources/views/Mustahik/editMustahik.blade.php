
@extends('layouts.app')
@section('content')

  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Mustahik
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Mustahik</a></li>
            <li><a href="{{url('data-donatur')}}">Data Mustahik</a></li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ url('update-mustahik/'.$data->id)}}" method="post">
                  <div class="box-body">
                  <div class="col-md-6">
                    <div class="form-group {{ $errors->has('namaMustahik') ? ' has-error' : '' }}">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="namaMustahik" value='{{$data->namaMustahik}}') }}">
                      
                    </div>
                     
                    
                     <div class="form-group {{ $errors->has('teleponMustahik') ? ' has-error' : '' }}">
                      <label>Nomor Telepon</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="teleponMustahik" value='{{$data->teleponMustahik}}') }}">
                       
                    </div>
                      
                    

                    <div class="form-group {{ $errors->has('alamatMustahik') ? ' has-error' : '' }}">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="alamatMustahik">{{$data->alamatMustahik}}</textarea>
                      
                    </div>
                   
                    <div class="form-group {{ $errors->has('pekerjaanMustahik') ? ' has-error' : '' }}">
                      <label>Pekerjaan</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="pekerjaanMustahik" value='{{$data->pekerjaanMustahik}}'>
                       
                    </div>
                    
                    
                    <div class="form-group">
                      
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name='checkbox'> Data yang saya isikan sudah benar,
                          </label>
                        </div>
                     
                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info pull-right">Simpan</button>
                    <button  class="btn btn-default pull-right" style="margin-right:10px">Cancel</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
                    
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
              </form>
              </div>
              </div>


            
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- /.content-wrapper -->

  


</div>
@endsection
@section('script')
<script type="text/javascript">
  //Date picker
    $('#Tanggal').datepicker({
      autoclose: true, 
      format :'dd MM yyyy',
      language : 'id',
    });


</script>
@endsection