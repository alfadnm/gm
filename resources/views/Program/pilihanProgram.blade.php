@extends('layouts.app')

@section('title', 'Pilihan Program')
@section('pilihan-program','active')
@section('program','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Pilihan Program
        <small>Pilihan Program yang tersedia</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Donatur</a></li>
        
        <li class="active">Pilihan Program</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <a href= "{{ url('tampilan-program')}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i>  Tambah Pilihan Program
              </a><br><br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Kode Program</th>
                    <th>Id Rekening</th>
                    <th>Nama Program</th>
                    <th>Target Dana</th>
                    <th>Keterangan</th>
                    
                   
                    <th> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                
               @foreach($data as $q=>$program)
                  <tr>
                    
                    <td>{{$program->kodeProgram}}</td>
                    <td>{{$program->idRekening}}</td>
                    <td>{{$program->namaProgram}}</td>
                    <td> {{$program->targetDana}}</td>
                     <td>{{$program->keterangan}}</td>

                    <td>
                     <div class="btn-group" role="group" aria-label="...">
                        
                        <a href="{{ url('edit-program/'.$program->id) }}" class="btn btn-sm btn-warning" style="margin-left: 5px">Edit</a>
                      
                        <a href="{{ url('delete-program/'.$program->id) }}" onclick="return confirm('Are you sure to delete this data?')" class="btn btn-sm btn-danger" style="margin-left: 5px">Hapus</a>
                      </div>
                    </td>
                  </tr>
                  @endforeach

                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
  <script >
     $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  </script>
@endsection

