@extends('layouts.app')
@section('title', 'Tambah Pilihan')
@section('pilihan-program','active')
@section('program','active')
@section('content')
  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Program
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Layanan Program</a></li>
            <li><a href="#">Edit Program</a></li>
            <li class="active">Tambah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ url('update-program/'.$data->id)}}" method="post">
                  <div class="box-body">
                  <div class="col-md-6">                    
                    
                    <div class="form-group {{ $errors->has('kodeProgram') ? ' has-error' : '' }}">
                      <label>Kode Program</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="kodeProgram" value="@if(old ('kodeProgram') != null) {{old ('kodeProgram') }} @else {{ $data->kodeProgram}} @endif">
                       @if ($errors->has('kodeProgram'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kodeProgram') }}</strong>
                        </span>
                      @endif
                    </div>

                    <div class="form-group {{ $errors->has('idRekening') ? ' has-error' : '' }}">
                      <label>ID Rekening</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="idRekening" value="@if(old ('idRekening') != null) {{old ('idRekening') }} @else {{ $data->idRekening}} @endif">
                       @if ($errors->has('idRekening'))
                        <span class="help-block">
                            <strong>{{ $errors->first('idRekening') }}</strong>
                        </span>
                      @endif
                    </div>

                    <div class="form-group {{ $errors->has('namaProgram') ? ' has-error' : '' }}">
                      <label>Nama Program</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="namaProgram" value="@if(old ('namaProgram') != null) {{old ('namaProgram') }} @else {{ $data->namaProgram}} @endif">
                       @if ($errors->has('namaProgram'))
                        <span class="help-block">
                            <strong>{{ $errors->first('namaProgram') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    
                    

                    
                    <div class="form-group {{ $errors->has('targetDana') ? ' has-error' : '' }}">
                      <label>Target Dana</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="targetDana" value="{{ $data->targetDana}}" >
                       @if ($errors->has('targetDana'))
                        <span class="help-block">
                            <strong>{{ $errors->first('targetDana') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('keterangan') ? ' has-error' : '' }}">
                      <label>Keterangan</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="keterangan" >@if(old ('keterangan') != null) {{old ('keterangan') }} @else {{ $data->keterangan}} @endif</textarea>
                       @if ($errors->has('keterangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('keterangan') }}</strong>
                        </span>
                      @endif
                    </div>
                    
                    <div class="form-group {{ $errors->has('checkbox') ? ' has-error' : '' }}">
                      
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="checkbox"> Data yang saya isikan sudah benar,
                          </label>
                        </div>
                         @if ($errors->has('checkbox'))
                        <span class="help-block">
                            <strong>{{ $errors->first('checkbox') }}</strong>
                        </span>
                      @endif
                     
                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info pull-right">Selesai</button>
                    <button  class="btn btn-default pull-right" style="margin-right:10px">Cancel</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
                    
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
              </form>
              </div>
              </div>


            
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- /.content-wrapper -->

 
</div>
<!-- ./wrapper -->
@endsection
@section('script')

@endsection
